package th.ac.mju.maejonavigationv2.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;

import th.ac.mju.maejonavigationv2.R;
import th.ac.mju.maejonavigationv2.activities.MainActivity;
import th.ac.mju.maejonavigationv2.adapters.CategoryAdapter;
import th.ac.mju.maejonavigationv2.listeners.RecyclerItemClickListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocationsFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<String> values = new ArrayList<String>();
    private ArrayList<String> filter = new ArrayList<String>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Firebase.setAndroidContext(getActivity());
        final View rootView = inflater.inflate(R.layout.fragment_locations, container,
                false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.location_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new CategoryAdapter(values);
        mRecyclerView.setAdapter(mAdapter);

        Firebase fb = new Firebase("https://maejo-navigator.firebaseio.com/");
        Firebase node = fb.child("locations/");
        Query locations = node.orderByKey();
        locations.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    values.add(ds.getKey().toString());
                }
                mAdapter.notifyDataSetChanged();
                filter = values;
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        String tag = ((MainActivity)getActivity()).getDetailsTag();
                        DetailsFragment sf = (DetailsFragment) getActivity().getSupportFragmentManager().findFragmentByTag(tag);
                        sf.renderDetails("key position : " + filter.get(position));
                        ((MainActivity) getActivity()).switchTabTo(2);

                    }
                })
        );

        return rootView;
    }

    public void filterLocation (String txt) {
        filter = new ArrayList<String>();
        for (String location : values) {
            final String text = location.toUpperCase();
            if (text.contains(txt)) {
                filter.add(location);
            }
        }
        mAdapter = new CategoryAdapter(filter);
        mRecyclerView.setAdapter(mAdapter);

    }
}
