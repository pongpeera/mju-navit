package th.ac.mju.maejonavigationv2.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;

import th.ac.mju.maejonavigationv2.R;
import th.ac.mju.maejonavigationv2.activities.MainActivity;
import th.ac.mju.maejonavigationv2.adapters.CategoryAdapter;
import th.ac.mju.maejonavigationv2.listeners.RecyclerItemClickListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends Fragment {


    private TextView tv;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<String> values = new ArrayList<String>();
    public CategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Firebase.setAndroidContext(getActivity());
        final View rootView = inflater.inflate(R.layout.fragment_category, container,
                false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.category_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        Firebase fb = new Firebase("https://maejo-navigator.firebaseio.com/");
        Firebase node = fb.child("category/");
        Query locations = node.orderByKey();
        locations.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                values = new ArrayList<String>();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    values.add(ds.getValue().toString());
                }
                mAdapter = new CategoryAdapter(values);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        mRecyclerView.addOnItemTouchListener(setClickListener());
        return rootView;
    }

    private RecyclerItemClickListener setClickListener() {
        return new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ((MainActivity)getActivity()).filterFromCategory( values.get(position) );
            }
        });
    }

}
