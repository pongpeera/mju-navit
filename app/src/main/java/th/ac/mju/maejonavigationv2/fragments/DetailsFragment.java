package th.ac.mju.maejonavigationv2.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import th.ac.mju.maejonavigationv2.R;
import th.ac.mju.maejonavigationv2.activities.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsFragment extends Fragment {
    private TextView tv;

    public DetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_details, container, false);
        String tag = getTag();
        ((MainActivity) getActivity()).setDetailsTag(tag);
        tv = (TextView) rootView.findViewById(R.id.location_name);
       // ImageView imgView = (ImageView) rootView.findViewById(R.id.location_img);
        //Glide.with(getActivity()).load(R.drawable.androidicon).into(imgView);
        return rootView;
    }

    public void renderDetails (String key) {
        tv.setText(key);
    }
}
