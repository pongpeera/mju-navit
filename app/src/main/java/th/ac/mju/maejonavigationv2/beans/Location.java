package th.ac.mju.maejonavigationv2.beans;

import java.io.Serializable;

/**
 * Created by Thammachad on 5/14/2016.
 */
public class Location implements Serializable {
    private double lng,lat;
    private String name,typemaker,details,imgPath;

    public Location( String name,String details,double lng, double lat,String typemaker, String imgPath) {
        this.lng = lng;
        this.lat = lat;
        this.name = name;
        this.typemaker = typemaker;
        this.details = details;
        this.imgPath = imgPath;
    }

    public double getLng() {
        return lng;
    }

    public double getLat() {
        return lat;
    }

    public String getName() {
        return name;
    }

    public String getTypemaker() {
        return typemaker;
    }

    public String getDetails() {
        return details;
    }

    public String getImgPath() {
        return imgPath;
    }
}
